'use strict';

class AppSettings {

	get baseUrl(){
		return 'https://globestamp.dyndns.org'
	};

	get clientId(){
		return '3QVrxqb21hlM4vJdqGDYeqITCK8nq1anYZpd4wBN'
	};

	get clientSecret(){
		return 'dU9NL4KXK4gyq2cNuKHouweSxwhEvqZs2E6MHqigkojIIc3JG0oFbS7YblVHG6dRHnlGbwCNNNOdN5ryYEqSiQ8oRvfDoSoy9gSFdoQyc7eJ3YNdClnN8hC1E4I6j0f3'
	};

	get grantType(){
		return 'password'
	};

	get accessToken(){
		return sessionStorage.getItem('accessToken');
	}

	set accessToken(accessToken){
		sessionStorage.setItem('accessToken', accessToken);
	}

	clearToken(){
		sessionStorage.removeItem('accessToken');
	}
};

class LoginView {
	constructor(appSettings, loginSuccessHandler){
		this.appSettings = appSettings;
		this.loginSuccessHandler = loginSuccessHandler;
		this.targetElem = $('#login-form');
		this.loginBtn = $('#do-login');
	}

	render(){
		let self = this;
		this.loginBtn.on('click', () => {self.doAuth()});
		this.targetElem.show();
	}

	hide(){
		this.targetElem.hide();
	}

	doAuth(){
		let url = `${this.appSettings.baseUrl}/auth/token/`;
		let credentials = {
			username: $('#username').val(),
			password: $('#password').val(),
			client_id: this.appSettings.clientId,
			client_secret: this.appSettings.clientSecret,
			grant_type: this.appSettings.grantType
		};

		$.ajax({
			type: 'POST',
			url: url,
			data: credentials,
			success: (response) => {this.authSuccess(response);},
			dataType: 'json'
		});
	}

	authSuccess(response){
		this.appSettings.accessToken = response.access_token;
		this.loginSuccessHandler();
	}
}

class UserDetailsView{
	constructor(appSettings){
		this.appSettings = appSettings;
		this.targetElem = $('#user-view');
	}

	render(){
		let url = `${this.appSettings.baseUrl}/user/me`;
		$('#do-logout').on('click', () => { this.doLogout(); });
		$.ajax({
			url: url,
			dataType:'json',
			headers: { Authorization: `Bearer ${this.appSettings.accessToken}`},
			success: (response) => { this.showUserDetails(response); }
		});

		this.targetElem.show();
	}

	hide(){
		this.targetElem.hide();
	}

	showUserDetails(userDetails){
		console.log(userDetails);
		$('#real-name').html(`${userDetails.first_name} ${userDetails.last_name}`);
		$('#num-trips').html(userDetails.trip_count);
	}

	doLogout(){
		this.appSettings.clearToken();
		window.location.reload();
	}
}

jQuery(() => {
	let appSettings = new AppSettings();
	let userDetailsView = new UserDetailsView(appSettings);
	let loginView = new LoginView(appSettings, () => { loginView.hide(); userDetailsView.render(); });


	if(appSettings.accessToken !== null){
		loginView.hide();
		userDetailsView.render();
	}else{
		userDetailsView.hide();
		loginView.render();
	}
});
